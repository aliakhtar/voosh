package io.voosh;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;
import com.google.common.collect.Lists;
import io.voosh.config.TableNames;
import io.voosh.shared.Logging;

import java.util.logging.Logger;

import static com.amazonaws.services.dynamodbv2.model.ScalarAttributeType.S;

class TableFactory
{
    private static final Logger log = Logging.get(TableFactory.class);

    private static ProvisionedThroughput throughput()
    {
        return new ProvisionedThroughput()
                       .withReadCapacityUnits(5L)
                       .withWriteCapacityUnits(6L);
    }

    private static KeySchemaElement partitionKeySchema(String name)
    {
        return new KeySchemaElement()
                       .withAttributeName(name)
                       .withKeyType(KeyType.HASH);
    }

    private static KeySchemaElement sortKeySchema(String name)
    {
        return new KeySchemaElement()
                       .withAttributeName(name)
                       .withKeyType(KeyType.RANGE);
    }

    private static AttributeDefinition attrib(String name, ScalarAttributeType type)
    {
        return new AttributeDefinition()
                       .withAttributeName(name)
                       .withAttributeType(type);
    }

    private static GlobalSecondaryIndex globalSecIndex(String name, String sortKey)
    {
        GlobalSecondaryIndex index = new GlobalSecondaryIndex();

        index.withIndexName(name + "-index");

        log.finer("Secondary index name: " + index.getIndexName());

        index.withProvisionedThroughput(throughput());
        index.withProjection(new Projection().withProjectionType(ProjectionType.ALL));

        if (sortKey != null)
            index.setKeySchema(Lists.newArrayList(partitionKeySchema(name), sortKeySchema(sortKey)));

        else
            index.setKeySchema(Lists.newArrayList(partitionKeySchema(name)));

        return index;
    }

    static void projects(AmazonDynamoDB db)
    {
        CreateTableRequest req = new CreateTableRequest();
        req.withTableName(TableNames.PROJECT);
        req.withProvisionedThroughput(throughput());
        req.withAttributeDefinitions( attrib("id", S));
        req.withKeySchema( partitionKeySchema("id"));

        db.createTable(req);
    }

    static void resources(AmazonDynamoDB db)
    {
        CreateTableRequest req = new CreateTableRequest();
        req.withTableName(TableNames.RESOURCE);
        req.withProvisionedThroughput( throughput() );

        req.withAttributeDefinitions( attrib("id", S), attrib("projectId", S));
        req.withKeySchema(partitionKeySchema("id"));

        req.withGlobalSecondaryIndexes( globalSecIndex("projectId", null) );

        db.createTable(req);
    }
}
