package io.voosh;

import com.almworks.sqlite4java.SQLite;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.local.embedded.DynamoDBEmbedded;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.testing.junit.DropwizardAppRule;
import io.dropwizard.util.Duration;
import io.voosh.config.VooshConfiguration;
import io.voosh.db.Db;
import io.voosh.shared.Logging;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

import static io.dropwizard.testing.ResourceHelpers.resourceFilePath;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public abstract class IntegrationTest
{
    @ClassRule
    public static DropwizardAppRule<VooshConfiguration> APP =
            new DropwizardAppRule<>(VooshApplication.class, resourceFilePath("voosh-test.json"));

    private final Logger log = Logging.get(this);

    protected VooshConfiguration config;
    protected final Client client;
    protected Db db;

    public IntegrationTest()
    {
        JerseyClientConfiguration clientConfig = new JerseyClientConfiguration();
        clientConfig.setTimeout(Duration.seconds(10));
        client = new JerseyClientBuilder(APP.getEnvironment())
                         .using(clientConfig)
                         .build("test client " + hashCode());
    }

    @SuppressWarnings("ConstantConditions")
    @BeforeClass
    public static void initEnvironment() throws Exception
    {
        SQLite.setLibraryPath(IntegrationTest.class.getClassLoader().getResource("sqlite").getPath());

        AmazonDynamoDB db = DynamoDBEmbedded.create();
        APP.getConfiguration().setDb( new Db(APP.getConfiguration(), db, new DynamoDBMapper(db)));
    }


    @Before
    public void cleanUp() throws Exception
    {
        config = APP.getConfiguration();
        db = config.getDb();

        AmazonDynamoDB dbClient = APP.getConfiguration().getDb().client();
        dbClient.listTables().getTableNames().forEach( dbClient::deleteTable );

        TableFactory.projects(dbClient);
        TableFactory.resources(dbClient);
    }

    @Before
    public void verifyEnvironment() throws Exception
    {
        APP.getEnvironment().healthChecks().runHealthChecks().entrySet().forEach(r ->
        {
            if ( r.getValue().isHealthy())
            {
                log.fine("Passed Health Check: " + r.getKey());
                return;
            }


            throw new IllegalStateException("Failed Health Check: " + r.getKey() + ", " + r.getValue().getMessage(),
                                                       r.getValue().getError());
        });
    }


    private String url(String path)
    {
        if (! path.startsWith("/assets"))
            path   = "/api/v1" + path;
        return "http://localhost:" + APP.getLocalPort() + path;
    }

    private String rawUrl(String path)
    {
        return "http://localhost:" + APP.getLocalPort() + path;
    }

    protected Response post(String path, Object entity)
    {
       return client.target(url(path)).request().post(Entity.json(entity));
    }

    protected Response get(String path)
    {
        return client.target( url(path) ).request().buildGet().invoke();
    }

    protected Response getRaw(String path)
    {
        return client.target( rawUrl(path) ).request().buildGet().invoke();
    }


    protected boolean validationFailed(Response resp)
    {
        return resp.getStatus() == 422;
    }

    protected void ensureUnprocessableEntityStatus(Response resp)
    {
        assertThat(resp.getStatusInfo().toString(),validationFailed(resp), is(true));
    }

    protected void ensureOk(Response resp)
    {
        assertThat(resp.getStatusInfo().toString(), resp.getStatus(), is(200));
    }


    protected void ensureUnauthorized(Response resp)
    {
        assertThat(resp.readEntity(String.class), resp.getStatus(), is(401));
    }

    protected String getHeader(String key, Response resp)
    {
        return String.valueOf(resp.getHeaders().getFirst( key ));
    }
}