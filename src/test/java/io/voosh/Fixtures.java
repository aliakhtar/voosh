package io.voosh;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;

import java.io.IOException;

import static io.dropwizard.testing.FixtureHelpers.fixture;

public class Fixtures
{

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    public static <T> T read(String path, Class<T> type) throws IOException
    {
        return MAPPER.readValue(fixture("fixtures/" + path + ".json"), type);
    }

}
