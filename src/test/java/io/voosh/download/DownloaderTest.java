package io.voosh.download;

import io.dropwizard.testing.FixtureHelpers;
import io.voosh.IntegrationTest;
import io.voosh.api.Resource;
import io.voosh.shared.Logging;
import io.voosh.shared.util.TestUtil;
import org.junit.Test;

import java.util.logging.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class DownloaderTest extends IntegrationTest
{
    private final Logger log = Logging.get(this);

    @Test
    public void call() throws Exception
    {
        Downloader dler = new Downloader(config, config.getClient(), TestUtil.HADRON_URL);
        Resource res = dler.call();
        assertThat(res, notNullValue() );

        assertThat(res.getContent().trim(), is(FixtureHelpers.fixture("download/hadron.txt").trim()));

        assertThat(res.getHeaders().isEmpty(), is(false) );
        assertThat(res.getContentType(), is("text/html") );

        res.getHeaders().forEach(h ->
        {
            assertThat(h.getName().trim().toLowerCase().equals("content-encoding"), is(false) );
        });
    }
}