package io.voosh.shared.util;


@FunctionalInterface
public interface Callback<T>
{
   void accept(T result);
}
