package io.voosh.shared.util;

import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import io.voosh.shared.Dtos;
import io.voosh.shared.ErrorsDto;

public interface Beans extends AutoBeanFactory
{
    AutoBean<ErrorsDto> errors();
    AutoBean<Dtos.OptimizeReq> optimizeReq();
    AutoBean<Dtos.Progress> progress();
}
