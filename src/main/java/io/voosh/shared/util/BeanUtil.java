package io.voosh.shared.util;

import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

public class BeanUtil
{
    public static Beans FACTORY;

    public static void setFactory(Beans beans)
    {
        FACTORY = beans;
    }

    public static <T> T decode(String json, Class<T> type)
    {
        return  AutoBeanCodex.decode(FACTORY, type, json)
                             .as();
    }

    public static <B> String encode(B bean )
    {
        return AutoBeanCodex.encode(AutoBeanUtils.getAutoBean(bean))
                            .getPayload();
    }
}
