package io.voosh.shared;

import java.util.logging.Logger;

public class Logging
{
    public static Logger get(Object o)
    {
        return Logger.getLogger( String.valueOf(o) );
    }
}
