package io.voosh.shared;

public enum Status
{
    ERROR,
    FINISHED,
    PROGRESS;
}
