package io.voosh;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.codahale.metrics.health.HealthCheck;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.websockets.WebsocketBundle;
import io.voosh.config.TableNames;
import io.voosh.config.VooshConfiguration;
import io.voosh.db.Db;
import io.voosh.resources.ProcessorResource;
import io.voosh.resources.ResourceServer;
import io.voosh.shared.Logging;
import io.voosh.vendor.HttpClient;
import server.StaticAssetsBundle;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import static com.codahale.metrics.health.HealthCheck.Result.healthy;
import static com.codahale.metrics.health.HealthCheck.Result.unhealthy;

public class VooshApplication extends Application<VooshConfiguration>
{
    private final Logger log = Logging.get(this);
    private WebsocketBundle wsBundle;

    public static void main(final String[] args) throws Exception
    {
        new VooshApplication().run(args);
    }

    @Override
    public String getName() {
        return "Voosh";
    }

    @Override
    public void initialize(final Bootstrap<VooshConfiguration> bootstrap)
    {
        bootstrap.addBundle(new StaticAssetsBundle("/assets/", "/"));

        wsBundle = new WebsocketBundle( Collections.emptyList(), Collections.emptyList() );
        bootstrap.addBundle(wsBundle);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void run(final VooshConfiguration config, final Environment env)
    {
        VooshConfiguration.INSTANCE = config;
        AmazonDynamoDB db = buildDb(config);
        config.setDb( new Db(config, db, new DynamoDBMapper(db)));

        config.setClient( new HttpClient(config));

        env.healthChecks().register("dynamo", dynamoHealthCheck(config));

        env.jersey().register(new ResourceServer(config));

        wsBundle.addEndpoint(ProcessorResource.class);
    }


    private AmazonDynamoDB buildDb(VooshConfiguration config)
    {
        BasicAWSCredentials awsCreds =
                new BasicAWSCredentials( config.getAwsConfig().getAccessKey(), config.getAwsConfig().getSecret() );

        AmazonDynamoDB db = new AmazonDynamoDBClient(awsCreds);
        db.setRegion(Region.getRegion(Regions.US_WEST_2));
        return db;
    }


    private HealthCheck dynamoHealthCheck(VooshConfiguration config)
    {
        Field[] tableNameFields = TableNames.class.getFields();
        List<String> tableNames = new ArrayList<>( tableNameFields.length );
        //Load table names via reflection. Each value of TableNames is loaded as a table to be checked.
        try
        {
            for (Field field : tableNameFields)
            {
                tableNames.add( field.get(null).toString() );
            }
        }
        catch (Exception e)
        {
            throw new IllegalStateException(e);
        }

        log.warning("Table names: " + tableNames);
        return new HealthCheck()
        {
            @Override
            protected Result check() throws Exception
            {
                AmazonDynamoDB db = config.getDb().client();
                List<String> existingTables = db.listTables().getTableNames();
                for (String table  : tableNames)
                {
                    if (!  existingTables.contains(table))
                        return unhealthy("Table not found in DynamoDb: "   + table);
                }
                return healthy("Tables: " + db.listTables().getTableNames().toString());
            }
        };
    }
}
