package io.voosh.db;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import io.voosh.shared.Logging;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

import static java.lang.String.format;
import static java.util.Optional.*;

public class DynamoDbCollection<T> implements DbCollection<T>
{
    private final Logger log = Logging.get(this);

    protected final Db db;
    protected final AmazonDynamoDB dbClient;
    protected final DynamoDBMapper mapper;

    protected final Class<T> type;

    protected final SimpleDateFormat dateFormatter;

    public DynamoDbCollection(Class<T> type, Db db)
    {
        this.db = db;
        this.dbClient = db.client();
        this.mapper = db.mapper();
        this.type = type;
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Override
    public void save(T object)
    {
        mapper.save(object);
    }

    @Override
    public void saveOrOverWrite(T object)
    {
        mapper.save(object, new DynamoDBMapperConfig(DynamoDBMapperConfig.SaveBehavior.CLOBBER));
    }

    @Override
    public List<T> scan()
    {
        return mapper.scan(type, new DynamoDBScanExpression());
    }

    @Override
    public Optional<T> first()
    {
        List<T> results = scan();
        return (! results.isEmpty()) ? of( results.get(0) ) : empty();
    }

    @Override
    public Optional<T> findBy(String partitionKey, Object val, Optional<String> indexName)
    {
        Map<String, AttributeValue> args = Collections.singletonMap(":v1", attribVal(val));

        DynamoDBQueryExpression<T> q = new DynamoDBQueryExpression<T>()
                       .withKeyConditionExpression(partitionKey + " = :v1")
                       .withExpressionAttributeValues(args).withConsistentRead(false);

        if (indexName.isPresent())
            q.setIndexName( indexName.get() );

        List<T> results = mapper.query(type, q);

        return (! results.isEmpty()) ? of( results.get(0) ) : empty();
    }

    @Override
    public Optional<T> find(String id)
    {
        T result = mapper.load(type, id);
        return ofNullable(result);
    }

    @Override
    public Optional<T> findBy(String partitionKey, String partitionKeyVal,
                              String sortKey, String sortKeyVal)
    {

        String expr = format("%s = :v1 and %s = :v2", partitionKey, sortKey);
        log.info("Querying: " + expr + " , " + partitionKeyVal + " , " + sortKeyVal);

        Map<String, AttributeValue> args = new HashMap<>(2);

        args.put(":v1", attribVal(partitionKeyVal));
        args.put(":v2", attribVal(sortKeyVal));

        DynamoDBQueryExpression<T> q = new DynamoDBQueryExpression<T>()
                                               .withKeyConditionExpression(expr)
                                               .withExpressionAttributeValues(args);

        List<T> results = mapper.query(type, q);

        return (! results.isEmpty()) ? of( results.get(0) ) : empty();
    }

    @Override
    public void delete(String id)
    {
        Optional<T> item = find(id);
        if (item.isPresent())
            delete(item.get());
        else
            log.warning("Delete called but item not found, id: " + id + " , type: " + type);

    }

    @Override
    public void delete(T item)
    {
        mapper.delete(item);
    }

    protected AttributeValue attribVal(Object val)
    {
        AttributeValue mapped = new AttributeValue();

        if (val == null)
            return mapped.withNULL(true);

        if (val instanceof String)
            mapped = mapped.withS( (String) val);

        else if (val instanceof Number)
            mapped = mapped.withN(String.valueOf(val));

        else if (val instanceof Boolean)
            mapped = mapped.withBOOL( (Boolean) val );

        else if (val instanceof Date)
        {
            mapped = mapped.withS( dateFormatter.format( (Date) val ) );
        }
        else
            throw new IllegalArgumentException("Unknown argument type: " + mapped);

        return mapped;
    }
}
