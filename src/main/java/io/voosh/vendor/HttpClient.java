
package io.voosh.vendor;

import io.voosh.config.VooshConfiguration;
import io.voosh.shared.Logging;
import org.glassfish.jersey.filter.LoggingFilter;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;
public class HttpClient
{
    private final Logger log = Logging.get(this);

    protected final VooshConfiguration config;
    protected final Client client = ClientBuilder.newClient();

    public HttpClient(VooshConfiguration config)
    {
        this.config = config;
    }

    public void enableLogging()
    {
        client.register(new LoggingFilter());
    }

    public  Response get(String url)
    {
        log.info("Making GET req: " + url);

        return client.target( url).request().buildGet().invoke();
    }

}
