package io.voosh.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AwsConfig
{
    private String accessKey;
    private String secret;

    @JsonProperty
    public String getAccessKey()
    {
        return accessKey;
    }

    @JsonProperty
    public void setAccessKey(String accessKey)
    {
        this.accessKey = accessKey;
    }

    @JsonProperty
    public String getSecret()
    {
        return secret;
    }

    @JsonProperty
    public void setSecret(String secret)
    {
        this.secret = secret;
    }

    @Override
    public String toString()
    {
        return "AwsConfig{" +
               "accessKey='" + accessKey + '\'' +
               ", secret='" + secret + '\'' +
               '}';
    }
}