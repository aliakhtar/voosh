package io.voosh.resources;

import io.voosh.config.VooshConfiguration;
import io.voosh.shared.Logging;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Path("p/{projectId}/{path: .* }")
public class ResourceServer
{
    private final Logger log = Logging.get(this);

    private final VooshConfiguration config;
    public ResourceServer(VooshConfiguration config)
    {
        this.config = config;
    }

    @GET
    public Response serve(@PathParam("projectId") String projectId, @PathParam("path") String path)
    {
        log.info("Received project id: " + projectId );
        log.info("Path: " + path);
        return Response.ok().build();
    }
}
