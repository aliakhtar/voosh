package io.voosh.client.route;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * RouteResolver.get()
 *    .create(Route.TASK)
 *      .add("add", "edit/:id", "view/:id)
 *      .param("id", new IsNumericRule() );
 *
 * RouteResolver.get()
 *    .create(Route.MSG)
 *      .add("view/:id", "reply/:id", "page/:num")
 *      .param("id", new isNumericRule(), new GetUserRequest() )
 *      .param("num", new IsNumericRule() );
 *
 *  RouteResolver.get()
 *     .create(Route.PROFILE)
 *        .add("for/:id")
 *         .param("id", new isNumericRule(), new GetUserRequest() )
 *
 *  RouteDef routeDef = RouteResolver.resolve( token );
 *  if ( routeDef.getRoute() == Route.PROFILE)
 *     profile( routeDef.params() );
 *
 *  void profile( RouteParams params)
 *  {
 *       long for = params.getLong("userId");
 *       UserDTO u = UserService.get(for);
 *       ProfilePresenter p = new ProfilePresenter( u );
 *  }
 */
public class RouteDef
{
    private final Route route;
    //private final List< List<RouteSegment> > segments = new ArrayList<>();
    private final Map<String, List<RouteSegment>> subRoutes = new HashMap<>(4);

    private RouteResult result;

    public RouteDef(Route route)
    {
        this.route = route;
        add( route.baseName() );
    }

    public Route route()
    {
        return route;
    }

    public RouteDef add(String... subRoutes)
    {
        for (String subRoute: subRoutes)
        {
            processSubRoute(subRoute);
        }
        return this;
    }


    private void processSubRoute(String subRoute)
    {
        List<RouteSegment> list = new ArrayList<>();
        for (String segment: subRoute.split("/") )
        {
            processSegment(segment, list);
        }
        subRoutes.put(subRoute, list);
    }

    private void processSegment(String segment, List<RouteSegment> list)
    {
        if (segment.isEmpty() )
            return;

        int index = list.size();
        list.add( new RouteSegment(index, segment) );
    }


    @SuppressWarnings(value = "unchecked")
    public <T> RouteDef param(String name)
    {
        addParamToSegments(new RouteParam(name));
        return this;
    }


    public RouteDef param(String name, boolean isRequired)
    {
        return param(name);
    }

    private void addParamToSegments(RouteParam<?> param)
    {
        for (String subRoute: subRoutes.keySet() )
        {
            List<RouteSegment> segments = subRoutes.get( subRoute );
            for (RouteSegment segment : segments)
            {
                if (! segment.isParam() )
                    continue;
                if (! segment.name().equals( param.name() ))
                    continue;

                segment.setParam( param );
            }
        }
    }


    /**
     * Token must be trimmed before being passed to this method or any other
     * methods in this class.
     */
    public boolean matches(String token)
    {
        if (token.isEmpty()  )
        {
            if (! route.baseName().isEmpty() )
                return false;
            result = new RouteResult(route);
            result.addSegment("");
            return true;
        }

        String[] segments = token.split("/");

        if (! segments[0].equals( route.baseName() ))
            return false;

        for (String subRoute : subRoutes.keySet() )
        {
            if (! matchesSubRoute(segments, subRoute))
                continue;

            return validate(segments, subRoute);
        }
        return false;
    }

    private boolean matchesSubRoute(String[] segments, String subRoute)
    {
        List<RouteSegment> rSegments = this.subRoutes.get( subRoute );

        if (segments.length > rSegments.size() )
            return false;

        if (segments.length < getRequiredSegmentCount(rSegments) )
            return false;

        //Loop through segments from 1, since element # 0 will be baseName,
        //e.g for the token: calendar/add/3 , element 0 is calendar which is the basename,
        //so ignore it and check against subRoute from element 1:
        for (int i = 0; i < rSegments.size(); i++)
        {
            RouteSegment segment = rSegments.get(i);
            if (segment.isParam() )
                continue;
            if (segment.isOptional() && segments.length <= i)
                return true;
            if (i >= segments.length)
                return false;
            String path = segments[i];
            if (! path.equals( segment.name() ))
                return false;
        }
        return true;
    }

    private int getRequiredSegmentCount( List<RouteSegment> segments )
    {
        int i = 0;
        for (RouteSegment segment : segments)
        {
            if (! segment.isOptional())
                i++;
        }
        return i;
    }


    private boolean validate(String[] segments, String subRoute)
    {
        result = new RouteResult(route);
        List<RouteSegment> rSegments = this.subRoutes.get( subRoute );
        for (int i = 0; i < rSegments.size(); i++)
        {
            result.addSegment( segments[i] );
            RouteSegment segment = rSegments.get(i);
            if (! segment.isParam() )
                continue;

            RouteParam<?> param = segment.getParam();
            if (param == null)
                return false;

            String value = segments[i];
            if ( (value == null || value.isEmpty() ) && segment.isOptional()  )
                continue;

            if (! param.validate( value) )
                return false;

            result.addParam(param);
        }
        return true;
    }

    public RouteResult getResult()
    {
        return result;
    }
}