package io.voosh.client.route;


import io.voosh.shared.Logging;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import static io.voosh.client.route.Front.INDEX;

public class RouteResolver
{
    private static final RouteResolver instance = new RouteResolver();

    private final Logger log = Logging.get(this);

    private final List<RouteDef> routeDefs = new ArrayList<>();

    private RouteResolver()
    {
        create(INDEX);
    }

    public static RouteResolver get()
    {
        return instance;
    }



    public RouteDef create(Route route)
    {
        RouteDef def = new RouteDef(route);
        routeDefs.add(def);

        return def;
    }

    public void remove(Route route)
    {
        Iterator<RouteDef> i = routeDefs.iterator();
        while (i.hasNext())
        {
            RouteDef def = i.next();
            if (def.route() == route)
            {
                i.remove();
            }
        }
    }

    public RouteResult resolve(String token)
    {
        if (token.startsWith("!")) //https://developers.google.com/webmasters/ajax-crawling/docs/getting-started
            token = token.substring(1);
        for (RouteDef def : routeDefs)
        {
            if (! def.matches(token))
                continue;
            return def.getResult();
        }

        return null;
    }
}
