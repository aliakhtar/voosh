package io.voosh.client.route;


public enum Front implements Route
{
    INDEX(""),
    ;

    private final String baseName;
    private String title;

    Front()
    {
        baseName = name().toLowerCase();
    }

    Front(String baseName)
    {
        this.baseName = baseName;
    }

    @Override
    public String baseName()
    {
        return baseName;
    }

    @Override
    public boolean isAvailable()
    {
        return true;
    }

    @Override
    public String title()
    {
        if (title == null)
            title = generateTitle(this);
        return title;
    }


    public String token(String subRoute)
    {

        return token() + "/" + subRoute;
    }

    private static String generateTitle(Front r)
    {
        switch (r)
        {
            case INDEX:
                return "Home";

            default:
                return ucFirst(r.name().toLowerCase());
        }
    }

    public static String ucFirst(String str)
    {
        return ucFirstX(str, 1);
    }

    public static String ucFirstX(String str, int toCapitalize)
    {
        String alphabeticPattern = ".*[a-zA-Z].*";
        int capitalized = 0;

        if (! str.matches(alphabeticPattern))
            return str;

        StringBuilder sb = new StringBuilder();
        int i;
        for (i = 0; i < str.length(); i++)
        {
            String chr = str.substring(i, i+1);
            boolean isAlphabetic = chr.matches(alphabeticPattern) ;
            chr = (! isAlphabetic) ? chr : chr.toUpperCase();
            sb.append(chr);
            if (isAlphabetic)
            {
                capitalized++;
                if (capitalized >= toCapitalize)
                    break;
            }
        }

        if (i < str.length())
            sb.append( str.substring(i + 1, str.length() ) );

        return sb.toString();
    }

}
