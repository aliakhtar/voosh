package io.voosh.client.route;

import com.google.gwt.http.client.URL;

public interface Route
{
    String TOKEN_CHAR = "#";

    public String name();

    public boolean isAvailable();

    public String baseName();
    public String title();

    public default String token()
    {
        return baseName();
    }
    public default String href()
    {
        return TOKEN_CHAR + baseName();
    }

    public default String token(String subRoute)
    {
        return token() + "/" + subRoute;
    }

    public static Route byToken(String token,
                                Route... routes)
    {
        for (Route r : routes )
        {
            if (token.equals( r.token() ) || token.startsWith( r.token() ))
                return  r;
        }
        throw new IllegalArgumentException("Route not found for token: " + token);
    }

    public static String encode(String input)
    {
        return URL.encodePathSegment(input);
    }

    public static String decode(String encoded)
    {
        return URL.decodePathSegment(encoded);
    }
}
