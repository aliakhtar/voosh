package io.voosh.client.ui;

import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.StyleInjector;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SimpleHtmlSanitizer;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.IsWidget;
import io.voosh.shared.Logging;

import java.util.logging.Logger;

/**
 * Utility methods for UI related tasks, like showing / hiding loading messages.
 */

public class Ui
{
    private static final NumberFormat formatter = NumberFormat.getFormat("##,###,###.##");
    private static final NumberFormat moneyFormatter = NumberFormat.getFormat("##,###,###");

    private static boolean inDrag = false;

    private static final Logger log = Logging.get(Ui.class);

    public static void init()
    {
        Event.addNativePreviewHandler(e ->
        {
            String type = e.getNativeEvent().getType();
            if (TouchStartEvent.getType().getName().equals(type))
            {
                log.info("Touch start, drag end");
                inDrag = false;
            }

            else if (TouchMoveEvent.getType().getName().equals(type))
            {
                log.info("Drag start");
                inDrag = true;
            }
        });
    }

    public static SafeHtml sanitize(String html)
    {
        return SimpleHtmlSanitizer.sanitizeHtml(html);
    }

    public static SafeHtml toSafeHtml(String html, boolean escape)
    {
        if (! escape)
            return new SafeHtmlBuilder().appendHtmlConstant( html ).toSafeHtml();
        else
            return new SafeHtmlBuilder().appendEscapedLines(html).toSafeHtml();
    }

    public static void addTouchHandler(final HasTouchEndHandlers widget)
    {
        widget.addTouchEndHandler(e ->
        {
            if (inDrag)
            {
                log.info("In drag, ignoring touchEnd");
                return;
            }

            triggerClick((HasClickHandlers) widget);
            e.preventDefault();
        });
    }

    public static void addTouchEnterHandler(HasTouchStartHandlers widget)
    {
        widget.addTouchStartHandler(e ->
        {
            triggerMouseOver((HasMouseOverHandlers) widget);
            e.preventDefault();
        });
    }

    public static void triggerClick(HasClickHandlers widget)
    {
        widget.fireEvent(new ClickEvent() {});
    }

    public static void triggerMouseOver( HasMouseOverHandlers widget )
    {
        widget.fireEvent(new MouseOverEvent(){});
    }

    public static void triggerMouseOut( HasMouseOutHandlers widget )
    {
        widget.fireEvent(new MouseOutEvent(){});
    }

    public static String format(Number number)
    {
        if (number == null)
            return "";
        return formatter.format(number);
    }

    public static String formatMoney(int money)
    {
        return moneyFormatter.format(money);
    }


    public static void placeHolder(IsWidget w, String placeHolder)
    {
        w.asWidget().getElement().setAttribute("placeholder", placeHolder);
    }

    public static void setAttr(IsWidget w, String attr, String value)
    {
        w.asWidget().getElement().setAttribute(attr, value);
    }


    public static Timer timer(Runnable callback)
    {
        return new Timer()
        {
            @Override
            public void run()
            {
                callback.run();
            }
        };
    }


    public static void injectJs(TextResource js)
    {
        ScriptInjector.fromString(js.getText())
                      .setWindow(ScriptInjector.TOP_WINDOW)
                      .inject();
    }

    public static void injectCss(TextResource css)
    {
        StyleInjector.inject(css.getText(), true);
    }


    /**
     * GWT port of jquery.visible plugin from:
     * https://github.com/customd/jquery-visible
     *
     * the .js file is still included in resources directory, but isn't
     * used in code.
     */
    public static boolean isVisible(Element e)
    {
        //vp seems to be viewPort, b = bottom, l = left, t = top, r = right, etc
        int vpWidth   = Window.getClientWidth();
        int vpHeight = Window.getClientHeight();

        //Ignoring clientSize as it only seems to be relavant if hidden
        //option is used.

        boolean tViz = ( e.getAbsoluteTop() >= 0 && e.getAbsoluteTop()<  vpHeight);
        boolean bViz = (e.getAbsoluteBottom() >  0 && e.getAbsoluteBottom() <= vpHeight);
        boolean lViz = (e.getAbsoluteLeft() >= 0 && e.getAbsoluteLeft() < vpWidth);
        boolean rViz = (e.getAbsoluteRight()  >  0 && e.getAbsoluteRight()  <= vpWidth);

        boolean vVisible   = tViz && bViz;
        boolean hVisible   = lViz && rViz;

        return hVisible && vVisible;
    }

    public static void showError(IsWidget item)
    {
        item.asWidget().addStyleName("err");
    }

    public static void hideError(IsWidget item)
    {
        item.asWidget().removeStyleName("err");
    }
}