package io.voosh.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.typedarrays.shared.ArrayBuffer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import io.voosh.client.VooshEntryPoint;
import io.voosh.client.ui.Form;
import io.voosh.client.ui.Ui;
import io.voosh.shared.Dtos;
import io.voosh.shared.Logging;
import io.voosh.shared.util.BeanUtil;
import org.realityforge.gwt.websockets.client.WebSocket;
import org.realityforge.gwt.websockets.client.WebSocketListener;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.logging.Logger;

import static io.voosh.client.VooshEntryPoint.prompt;

public class IndexView extends BaseView implements WebSocketListener
{
    interface ViewUiBinder extends UiBinder<Panel, IndexView> {};
    private static final ViewUiBinder binder = GWT.create(ViewUiBinder.class);

    private final Logger log = Logging.get(this);

    @UiField Form form;
    @UiField TextBox url;
    @UiField Button btn;

    @UiField Element progress;
    @UiField Element updateText;
    @UiField Element progressText;

    @UiField Element results;

    @UiField Panel reqPanel;
    @UiField Panel progressPanel;
    @UiField Panel successPanel;

    private final WebSocket ws;
    private Dtos.OptimizeReq pendingReq;

    public IndexView()
    {
        ws = WebSocket.newWebSocketIfSupported();

        init(binder.createAndBindUi(this));
        Ui.setAttr(url, "placeholder", "Enter a site, e.g example.com");

        btn.addClickHandler(e -> sendToServer());
        form.setOnSubmit(this::sendToServer);

        if (ws == null)
        {
            prompt("You're using an ancient web browser. Please join the rest of us in 2016 and use a modern web browser that supports Websockets");
            return;
        }

        ws.setListener(this);
        log.info("Connecting to server ");
        ws.connect( VooshEntryPoint.get().config().wsUrl() + "/process" );
    }

    @Override
    public String pageStyles()
    {
        return "";
    }

    private void sendToServer()
    {
        reqPanel.setVisible(false);
        progressPanel.setVisible(true);

        pendingReq = BeanUtil.FACTORY.optimizeReq().as();
        pendingReq.setUrl(url.getValue());
        if (! pendingReq.getUrl().toLowerCase().startsWith("http"))
            pendingReq.setUrl( "http://" + url.getValue() );
        log.info("Set url");

        if (ws.isConnected())
            sendPendingReq();
    }

    private void sendPendingReq()
    {
        ws.send(BeanUtil .encode( pendingReq ));
        log.info("Sent: " + BeanUtil .encode( pendingReq ));
        pendingReq =  null;
    }

    @Override
    public void onOpen(@Nonnull WebSocket webSocket)
    {
        log.info("Ws opened");
        if (pendingReq != null)
        {
            log.info("Sending pending req: " + pendingReq);
            sendPendingReq();
        }
    }

    @Override
    public void onClose(@Nonnull WebSocket webSocket, boolean wasClean, int code, @Nullable String reason)
    {
        log.warning("Ws closed");
    }

    @Override
    public void onMessage(@Nonnull WebSocket webSocket, @Nonnull String data)
    {
        log.info("Got msg: " + data);
        Dtos.Progress update = BeanUtil.decode(data, Dtos.Progress.class);
        switch (update.getStatus())
        {
            case ERROR:
                prompt(update.getMessage());
                progressPanel.setVisible(false);
                reqPanel.setVisible(true);
                break;

            case PROGRESS:
                updateText.setInnerText( update.getMessage() );
                progressText.setInnerText( update.getProgress() + "%" );
                progress.getStyle().setWidth( (double) update.getProgress(), Style.Unit.PCT);
                break;

            case FINISHED:
                finish(update);
                break;
        }
    }

    private void finish(Dtos.Progress update)
    {
        String ugly = ""; //fixme please
        for (String url : update.getResults().keySet())
        {
            ugly += buildRowString(url, update.getResults().get(url));
        }

        if (ugly.isEmpty())
            prompt("Looks like there were no links found on the site you provided, or the world has ended and we're all zombies.");

        results.setInnerHTML(ugly);

        progressPanel.setVisible(false);
        successPanel.setVisible(true);
    }

    @Override
    public void onMessage(@Nonnull WebSocket webSocket, @Nonnull ArrayBuffer data)
    {
        prompt("da faq, " + data.toString());
    }

    @Override
    public void onError(@Nonnull WebSocket webSocket)
    {
        log.warning("Ws error");
        prompt("There was an error communicating with the server");
    }

    //Ew ew ew ew ew fixme
    private String buildRowString(String url, String title)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("<tr><td>").append(title).append("</td>");
        sb.append("<td><a href='").append(url).append("' class='orange button' target='_NEW'>View</a>").append("</td></tr>");

        return sb.toString();
    }
}