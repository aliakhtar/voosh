package io.voosh.client.service;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.RestService;

public interface ApiService<I, O> extends RestService
{
    void makeReq(I input, MethodCallback<O> output);
}
