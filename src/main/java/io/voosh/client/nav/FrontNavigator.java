package io.voosh.client.nav;


import io.voosh.client.view.IndexView;
import io.voosh.shared.Logging;

import java.util.logging.Logger;

import static io.voosh.client.route.Front.INDEX;

public class FrontNavigator
{
    private static FrontNavigator instance;
    private final Navigator nav;
    private final Logger log = Logging.get(this);

    public static void init()
    {
        Navigator nav = Navigator.get();
        instance = new FrontNavigator(nav);

        nav.addHandler(INDEX, r -> instance.home() )
        ;
    }

    private FrontNavigator(Navigator nav)
    {
        this.nav = nav;
    }

    private void home()
    {
        nav.end(INDEX, new IndexView());
    }

}