package io.voosh.client.res;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;

public interface Res extends ClientBundle
{
    Res INSTANCE = GWT.create(Res.class);
}
